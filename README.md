# Project Name

This project automates the deployment of containerized applications using GitOps principles.

## Overview
The project includes a YAML template file `deployDockerCompose.yml` that defines the configuration inputs and pipeline stages for deploying container images to a GitOps project.

## Template Specifications for gitops-pullToDockerCompose

### `spec:`
- `gitlab_trigger_token`:
    - Type: string
    - Description: The GitLab trigger token that allows triggering the pipeline in another project.
- `branch`:
    - Type: string
    - Default: main
    - Description: The branch where the pipeline will be triggered.
- `image_name`:
    - Type: string
    - Description: The name of the image to be replaced in the gitops-project (e.g., yourName/yourProject from your container-registry).
- `project_directory`:
    - Type: string
    - Description: The directory of the gitops-project.
- `configuration_file`:
    - Type: string
    - Description: The name of the configuration file (e.g., docker-compose.yaml).
- `service_name`:
    - Type: string
    - Description: The name of the service to be replaced in the gitops-project inside the configuration file.
- `target_project`:
    - Type: string
    - Description: The ID of the project of the gitops-project that includes the configurations.

## Usage
To use this template, update the specified input values in the `deployDockerCompose.yml` file and integrate it into your GitLab CI/CD pipeline.

The easiest way is, to import this gitlab catalog component in your project e.g.

```
include:
- component: gitlab.com/Laszlo.Lueck/gitops-pulltotarget/deployDockerCompose@version
    inputs:
      branch: "the branch to push in the gitops repository"
      image_name: "registryName/registry"
      configuration_file: "docker-compose.yaml"
      service_name: "service name as recorded in docker-compose.yaml to identify the appropriate service"
      project_directory: "if you have multiple directories in your gitops project"
      target_project: the id of the gitops project
      gitlab_trigger_token: "the trigger token, created in the gitops project, to allowing push and trigger jobs from here"
```

Then, you can use a normal stage for processing the component e.g.

```
gitops-pullToDockerCompose:
  stage: update:gitops
  ... other things ...
  before_script:
    - source $VARIABLES_FILE <-- this one holds a variable $DOCKER_TAG, we must inherited dynamically from the stage
```

## Template Specifications for gitops-pullToHelm

### `spec:`
- `gitlab_trigger_token`:
  - Type: string
  - Description: The GitLab trigger token that allows triggering the pipeline in another project.
- `branch`:
  - Type: string
  - Default: main
  - Description: The branch where the pipeline will be triggered.
- `project_directory`:
  - Type: string
  - Description: The directory of the gitops-project.
- `target_project`:
  - Type: string
  - Description: The ID of the project of the gitops-project that includes the configurations.

## Usage
To use this template, update the specified input values in the `deployHelm.yml` file and integrate it into your GitLab CI/CD pipeline.

The easiest way is, to import this gitlab catalog component in your project e.g.

```
include:
- component: gitlab.com/Laszlo.Lueck/gitops-pulltotarget/deployHelm@version
    inputs:
      branch: "the branch to push in the gitops repository"
      project_directory: "if you have multiple directories in your gitops project"
      target_project: the id of the gitops project
      gitlab_trigger_token: "the trigger token, created in the gitops project, to allowing push and trigger jobs from here"
```

Then, you can use a normal stage for processing the component e.g.

```
gitops-pullToHelm:
  stage: update:gitops
  ... other things ...
  before_script:
    - source $VARIABLES_FILE <-- this one holds a variable $DOCKER_TAG, we must inherited dynamically from the stage
```

Ready!